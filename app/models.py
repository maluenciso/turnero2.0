# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Personas(models.Model):
    id = models.BigAutoField(primary_key=True)
    numero_documento = models.CharField(max_length=20)
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    telefonos = models.CharField(max_length=100, blank=True, null=True)
    direccion = models.CharField(max_length=50, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    ultima_modificacion = models.DateTimeField(blank=True, null=True)
    fecha_creacion = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.numero_documento

    class Meta:
        db_table = 'personas'

class Servicios(models.Model):
    id = models.BigAutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)

    def __str__(self):
        return self.descripcion
    class Meta:
        db_table = 'servicios'   

class Prioridades(models.Model):
    id = models.BigAutoField(primary_key=True)
    condicion = models.CharField(max_length=200)
    prioridad = models.IntegerField()
    ultima_modificacion = models.DateTimeField()
    fecha_creacion = models.DateTimeField()

    def __str__(self):
        return self.prioridad
    class Meta:
        db_table = 'prioridades'   

class BocasAtencion(models.Model):
    id = models.BigAutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)
    activo = models.BooleanField()
    servicio = models.ForeignKey('Servicios', on_delete=models.CASCADE)

    def __str__(self):
        return self.descripcion
    class Meta:
        db_table = 'bocas_atencion'


class Clientes(models.Model):
    id = models.BigAutoField(primary_key=True)
    ultima_modificacion = models.DateTimeField()
    fecha_creacion = models.DateTimeField()
    persona = models.ForeignKey('Personas', on_delete=models.CASCADE)
   
 

    def __str__(self):
        return self.persona
    class Meta:
        db_table = 'clientes'


class Usuarios(models.Model):
    id = models.BigAutoField(primary_key=True)
    rol = models.CharField(max_length=20)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=100)
    ultima_modificacion = models.DateTimeField()
    fecha_creacion = models.DateTimeField()
    activo = models.BooleanField()
    persona = models.ForeignKey(Personas, on_delete=models.CASCADE)
    

    def __str__(self) :
        return self.username
    class Meta:
        db_table = 'usuarios'

class Turnos(models.Model):
    id = models.BigAutoField(primary_key=True)
    usuario_asesor = models.ForeignKey('Usuarios', on_delete=models.CASCADE)
    fecha_hora_llegada = models.DateTimeField()
    fecha_hora_llamada = models.DateTimeField(blank=True, null=True)
    fecha_hora_finalizacion = models.DateTimeField(blank=True, null=True)
    estado = models.CharField(max_length=50)
    prioridad = models.ForeignKey(Prioridades, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Clientes, on_delete=models.CASCADE)
    usuario_atencion = models.ForeignKey('Usuarios',related_name="usuario_atencion", on_delete=models.CASCADE)
    boca_atencion = models.ForeignKey(BocasAtencion, on_delete=models.CASCADE)

    def __str__(self):
        return self.fecha_hora_llamada
    class Meta:
        db_table = 'turnos'